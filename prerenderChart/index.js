const fs = require('fs')
const path = require('path')
const jsdom = require('jsdom')
const { JSDOM } = jsdom
const dom = new JSDOM(`<!DOCTYPE html><div id="dataviz-container"></div>`)
global.wondow = dom.window
global.document = dom.window.document
const { getNodesAndLinksForSankey } = require('../docs/isomorphic-js/datagen')
const allMoviesNodes = require('../docs/assets/topics.json')
// const moviesNodes = require('../docs/assets/topics.json')
const { prepareContainers } = require('../docs/isomorphic-js/plot')

const chartContainer = dom.window.document.querySelector('#dataviz-container')
const moviesNodes = [];
allMoviesNodes.forEach(function(node, index){
	if(node.influencedBy.length > 0){
		moviesNodes.push(node);
	}
})
const data = getNodesAndLinksForSankey(moviesNodes)
console.log('Data Loaded')
// console.log(data)
const chartHeight = 7000


prepareContainers(data, chartContainer, chartHeight)
console.log('Containers prepared')
const renderedHTMLPath = path.resolve(__dirname, '../docs/assets/renderedChart.html')
fs.writeFileSync(renderedHTMLPath, chartContainer.innerHTML)
console.log(`Successfully written html to ${renderedHTMLPath}`)
