const d3 = require('d3')
const _ = require('lodash')
d3.sankey = require('d3-sankey').sankey

// function plotChart(svg, width, height, data) {
//   console.log('Inside plotChart')
//   const formatNumber = d3.format(',.0f')
//   const format = d => `${formatNumber(d)} TWh`
//   const color = d3.scaleOrdinal(d3.schemeCategory20)

//   const sankey = d3.sankey()
//     .nodeWidth(data.nodeWidth || 20)
//     .nodePadding(data.nodePadding || 15)
//     .size([width, height])

//   const path = sankey.link(10)

//   // We chose to not to polute the actual data
//   const nodes = _.cloneDeep(data.nodes)
//   const links = _.cloneDeep(data.links)

//   console.log('Calling Sankey') 
  
//   sankey
//     .nodes(data.nodes)
//     .links(data.links)
//     .layout(32)

//   console.log('After Sankey')
//   const tip = d3.tip()
//     .attr('class', 'd3-tip')
//     .offset([-10, 0])
//     .html(d => `Topic ${d.topic}`)
//   console.log('Adding tip')  
//   svg.call(tip)
//   const link = svg.append('g').selectAll('.link')
//     .data(data.links)
//     .enter()
//     .append('path')
//     .attr('class', d => `link ${d.cssClasses.join(' ')}`)
//     .attr('d', path)
//     // .style('stroke-width', d => (d.type === 'year-link' ? 0 : 1))
//     .sort((a, b) => b.dy - a.dy)

//   link.append('title')
//     .text(d => `${d.source.name}  →  ${d.target.name} ${'\n'} ${format(d.value)}`)

//   const node = svg.append('g').selectAll('.node')
//     .data(data.nodes)
//     .enter()
//     .append('g')
//     .attr('class', d => `node ${d.cssClasses.join(' ')}`)
//     .attr('transform', d => `translate(${d.x},${d.y})`)


//   /*
//       node.call(d3.behavior.drag()
//         .origin(function (d) { return d; })
//         .on('dragstart', function () { this.parentNode.appendChild(this); })
//         .on('drag', dragmove));
//   */
//   node.append('rect')
//     .attr('height', d => (d.dy < 10 ? 10 : d.dy))
//     .attr('width', sankey.nodeWidth())
//     .attr('class', 'node-rect')
//     .style('fill', (d) => {
//       d.color = (d.type === 'timeline-node') ? '#c7c7c7' : color(d.name.replace(/ .*/, ''))
//       return d.color
//     })
//     .style('stroke', d => d3.rgb(d.color).darker(2))
//     .append('title')
//     .text(d => `${d.topicMilestonePaperLink}`)

//   // Open a new window on click
//   node.on('click', (d) => {
//     //window.open(d.topicMilestonePaperLink, '_newtab')
//     window.submit(d.topic)
//   })

//   // text
//   const nodeText = node.append('text')
//     .attr('x', -6)
//     .attr('y', d => d.dy / 2)
//     .attr('dy', '.35em')
//     .attr('text-anchor', 'end')
//     .attr('transform', null)
//     .text(d => ((d.type === 'timeline-node') ? '' : `${d.topic}`))
//     // .filter(d => d.x < width / 2)
//     .attr('x', 6 + sankey.nodeWidth())
//     .attr('text-anchor', 'start')
//     .attr('class', d => `node-id-${d.id}`)

//   nodeText.append('tspan')
//     .attr('x', (d => ((d.type === 'timeline-node') ? -5 : 25)))
//     .attr('dy', (d => ((d.type === 'timeline-node') ? 22 : 15)))
//     .text(d => ((d.type === 'timeline-node') ? d.year : `(${d.year})`))

//   // Gather relative links and store references for hover - highlighting effects
//   node.each((d) => {
//     d.associatedLinks = d3.selectAll(`.link-node-id-${d.timeSort}`)
//     d.associatedNodes = d3.selectAll(`.node-node-id-${d.timeSort}`)
//   })
//   // highlight / de highlight relative links
//   node.filter(d => (d.type !== 'timeline-node'))
//     .on('mouseenter', (d) => {
//       if (d.type !== 'year-node') {
//         d.associatedLinks.classed('active-link', true)
//         d.associatedNodes.classed('active-node', true)
//       }
//     }).on('mouseleave', (d) => {
//       if (d.type !== 'year-node') {
//         d.associatedLinks.classed('active-link', false)
//         d.associatedNodes.classed('active-node', false)
//       }
//     })

//   node.filter(d => (d.type !== 'timeline-node'))
//     .on('mouseenter', tip.show).on('mouseleave', tip.hide)

//   // remove year nodes and links
//   d3.selectAll('.year-node').remove()
//   d3.selectAll('.year-link').remove()
// }


function plotChart(svg, width, height, data) {
  console.log('Inside plotChart')
  console.log(width)
  const formatNumber = d3.format(',.0f')
  const format = d => `${formatNumber(d)} TWh`
  const color = d3.scaleOrdinal(d3.schemeCategory20)
  const sankey = d3.sankey()
    .nodeWidth(data.nodeWidth || 20)
    .nodePadding(data.nodePadding || 20)
    .size([width, height])

  const path = sankey.link(10)
  // We chose to not to polute the actual data
  const nodes = _.cloneDeep(data.nodes)
  const links = _.cloneDeep(data.links)

  sankey
    .nodes(nodes)
    .links(links)
    .layout(32)

  const link = svg.append('g').selectAll('.link')
    .data(links)
    .enter()
    .append('path')
    .attr('class', d => `link ${d.cssClasses.join(' ')}`)
    .attr('d', path)
    // .style('stroke-width', d => (d.type === 'year-link' ? 0 : 1))
    .sort((a, b) => b.dy - a.dy)

  link.append('title')
    .text(d => `${d.source.name}  →  ${d.target.name} ${'\n'} ${format(d.value)}`)


  const node = svg.append('g').selectAll('.node')
    .data(nodes)
    .enter()
    .append('g')
    .attr('class', d => `node ${d.cssClasses.join(' ')}`)
    .attr('transform', d => `translate(${d.x},${d.y})`)

  node.append('rect')
    .attr('height', d => (d.dy < 10 ? 10 : d.dy))
    .attr('width', sankey.nodeWidth())
    .attr('class', 'node-rect')
    .style('fill', (d) => {
      d.color = (d.type === 'timeline-node') ? '#c7c7c7' : color(d.name.replace(/ .*/, ''))
      return d.color
    })
    .style('stroke', d => d3.rgb(d.color).darker(2))
    .append('title')
    .text(d => `${d.topicMilestonePaperLink}`)

  // text
  const nodeText = node.append('text')
    .attr('x', -6)
    .attr('y', d => d.dy / 2)
    .attr('dy', '.35em')
    .attr('text-anchor', 'end')
    .attr('transform', null)
    .text(d => ((d.type === 'timeline-node') ? '' : `${d.topic}`))
    // .filter(d => d.x < width / 2)
    .attr('x', 6 + sankey.nodeWidth())
    .attr('text-anchor', 'start')
    .attr('class', d => `node-id-${d.timeSort}`)

  nodeText.append('tspan')
    .attr('x', (d => ((d.type === 'timeline-node') ? -5 : 25)))
    .attr('dy', (d => ((d.type === 'timeline-node') ? 22 : 15)))
    .text(d => ((d.type === 'timeline-node') ? d.year : `(${d.year})`))

  d3.selectAll('.year-node').remove()
  d3.selectAll('.year-link').remove()
}


function prepareContainers(data, chartContainer, chartHeight) {
  console.log('Inside prepareContainers')
  const margin = { top: 1, right: 1, bottom: 6, left: 1 }
  const width = (data.allYears.length * 120) - margin.left - margin.right + 100
  const height = chartHeight - margin.top - margin.bottom


  const moviesSVG = d3.select(chartContainer).append('svg')
    .attr('width', width + margin.left + margin.right + 200)
    .attr('height', height + margin.top + margin.bottom)
    .attr('id', 'moviesSVG')
    .append('g')
    .attr('transform', `translate(${margin.left + 10},${margin.top})`)

  console.log('moviesSvg')
  const timelineSVG = d3.select(chartContainer).append('svg')
    .attr('width', width + margin.left + margin.right + 200)
    .attr('height', 50)
    .attr('id', 'timelineSVG')
    .append('g')
    .attr('transform', `translate(${margin.left + 10}, 10)`)

  console.log('Calling plotChart')
  plotChart(moviesSVG, width, height, {
    nodes: data.nodes,
    links: data.links,
  })

  console.log('Calling second plotChart')
  plotChart(timelineSVG, width, 20, {
    nodes: data.timeLineNodes,
    links: data.timeLineLinks,
  })

  console.log('Containers prepared from plot.js')
  // These returned refernces would be used in the browser to add interactivity to the nodes in the chart
  return {
    moviesSVG,
    timelineSVG,
  }



}
module.exports = {
  plotChart,
  prepareContainers,
}
